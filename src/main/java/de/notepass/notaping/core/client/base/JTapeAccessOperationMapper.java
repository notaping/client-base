package de.notepass.notaping.core.client.base;

import de.notepass.notaping.core.access.jTapeAccess.model.BlockSizeCorrectingTapeOutputStream;
import de.notepass.notaping.core.access.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeException;
import de.notepass.notaping.core.fs.etfs.action.model.TapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.data.metadata.HeaderMetaDataField;
import de.notepass.notaping.core.shared.WrittenBytesAwareOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class JTapeAccessOperationMapper implements TapeSpecificOperationDispatcher {
    private SimpleTapeDrive tapeDrive;

    public JTapeAccessOperationMapper(SimpleTapeDrive tapeDrive) {
        this.tapeDrive = tapeDrive;
    }

    public SimpleTapeDrive getTapeDrive() {
        return tapeDrive;
    }

    @Override
    public HeaderMetaDataField.HEADER_METADATA_FLAG[] getCapabilities() {
        //return new HeaderMetaDataField.HEADER_METADATA_FLAG[]{HeaderMetaDataField.HEADER_METADATA_FLAG.EOF_SUPPORT};
        return new HeaderMetaDataField.HEADER_METADATA_FLAG[0];
    }

    @Override
    public boolean isEndOfTapeException(Exception e) {
        if (e instanceof TapeException) {
            return ((TapeException) e).getErrorCode() == TapeException.ErrorCode.NO_SPACE_LEFT
                    || ((TapeException) e).getErrorCode() == TapeException.ErrorCode.END_OF_TAPE_REACHED;
        }

        return false;
    }

    @Override
    public InputStream getCurrentInputStream() throws IOException {
        return tapeDrive.getInputStream();
    }

    @Override
    public WrittenBytesAwareOutputStream getCurrentOutputStream() throws IOException {
        return tapeDrive.getOutputStream();
    }

    @Override
    public void writeFilemarks(int amount) throws TapeException {
        tapeDrive.writeFilemarks(amount);
    }

    @Override
    public int setBytePosition(long byteIndex) throws TapeException {
        long blockSize = tapeDrive.getTapeInfo().getBlockSizeBytes();
        long blockIndex = (long) Math.floor(((double)byteIndex) / blockSize);
        int blockOffset = (int) (byteIndex - (blockIndex * blockSize));

        tapeDrive.setTapeBlockPosition(blockIndex);

        return blockOffset;
    }

    @Override
    public WrittenBytesAwareOutputStream getTemporaryHeaderOutputStream() throws IOException {
        return new BlockSizeCorrectingTapeOutputStream(tapeDrive.getRawOutputStream(), (int) tapeDrive.getTapeInfo().getBlockSizeBytes());
    }

    @Override
    public void discardTemporaryHeaderOutputStream(WrittenBytesAwareOutputStream os) throws IOException {

    }
}
